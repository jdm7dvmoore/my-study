We've discussed some of the ideas of the linearity of quantum mechanics already for the time
independent Schroedinger equation. There, because the equation is linear in a specific,
mathematical way, we can multiply any solution by a constant. And it is still a solution.
That aspect of linearity allowed us to normalize our wave function solutions, for example.
And of course, that is very convenient for the algebra.
There is a second aspect of linearity which is linear superposition. Linear superposition
is the idea that if we have two different solutions of an equation, then the sum of
them is also a solution. That aspect of linearity does not arise so often with the time independent
Schroedinger equation.
It can only arise when we have what are called the degenerate solutions. That is, more than
one orthogonal eigenfunction for a given eigenvalue. Though that particular situation does arise
quite often in highly symmetric problems. And we will certainly get back to that when
we consider the solutions to the hydrogen atom problem.
For solutions to the time dependent Schroedinger equation-- which is also a linear equation--
superposition arises all the time. Remember, we said that at a given point in time it is
possible for any function in space, as long it has a well-behaved second derivative, to
be a solution.
Because of linearity, the sum of such solutions are also solutions. And so there are few constraints
on our ability to construct linear superpositions of different solutions-- superpositions that
will themselves be solutions.
This mathematical fact is very useful in helping us solve for the possible behaviors in time
as we shall see.
The time dependent Schroedinger equation is linear in the wave function, the wave function
amplitude psi. One reason that we can see it's linear is that we see no higher powers
of psi anywhere in this equation. We've got psi here, we've got psi there. We've got psi
here. But we have no psi squared or psi cubed.
A second reason why this equation is called linear is that psi appears in every term.
There's no additive constant term anywhere. And that allows this equation to be linear
in the full sense of what we mean by that term.
Explicitly, linearity requires two conditions both of which are obeyed by this particular
equation, Schroedinger's time dependent equation. The first condition is that if psi is a solution,
then so also is a times psi, where a is any constant. It can be real or imaginary or complex.
Secondly, if psi a and psi b are both solutions, then so also is the sum of those solutions.
A consequence of these two conditions taken together is that a wave function that is the
sum of ca times one solution plus cb times the other solution-- where ca and cb can be
complex-- is also a solution. So that's a simple consequence of these two conditions.
The fact that this is also a solution if psi a and psi b are solutions is the property
of linear superposition. To emphasize, linear superpositions of solutions of the time dependent
Schroedinger equation are themselves also solutions.
The concept of linear superposition solutions is common in the classical world when we're
talking about waves and vibrations. But we're not used to thinking about particles being
in linear superpositions.
In classical mechanics, a particle simply has a state that is defined by its position
and its momentum. Now we're saying that a particle may exist in a superposition of states,
each of which may have different energies, or possibly different positions and momenta.
Again, we might think this is an odd situation, and maybe an unusual one, to encounter even
in quantum mechanics. In fact, for systems that are not static in time-- in other words,
in systems where things are changing-- such superpositions are actually necessary in quantum
mechanics, so that we can recover the behavior we expect classically from particles.
This becomes particularly obvious once we start looking at the behavior in terms of
superpositions of eigenstates.
This possibility of linear superpositions has a very important consequence, which we
will see, and which allows us to exploit the mathematics of eigenstates we've already understood.
We know that if the potential V is constant in time, each of the energy eigenstates--
psi n of r-- with eigenenergy En is separately a solution of the time dependent Schroedinger
equation, provided we remember to multiply it by the right complex exponential factor.
That is, provided we multiply our solutions, our eigensolutions, of the time independent
equation by the e to the minus i Ent over h bar where En is the eigenvalue, energy eigenvalue.
Then this is also a solution of the time dependent Schroedinger equation.
Now we also know that the set of eigenfunctions of problems we will consider, these eigenfunctions
of the time independent Schroedinger equations, that set is a complete set.
So the wave function at time t equals zero can be expanded in them. That is, the wave
function at time t equals zero, psi of r and times zero, can be written out as some linear
combination, some expansion in the solutions to the time independent Schroedinger equation.
And an here are just our expansion coefficients.
And we know how to work these out of course, given this particular function. But we know
that a function that starts out as psi n of r will evolve in time according to this simple
multiplicative factor here. So if it's psi n of r at time t equals zero, then at future
times that function will simply turn into this one.
And so by linear superposition, the solution at time t, later, is just this sum here. So
we knew the solution at time t equals zero. This was the wave function we started with
for some reason. We managed to expand that in the set of functions that are the energy
eigenfunctions for the same potential. And we're presuming the potential is constant.
And then, having done this, we know what the result is going to be at all future times
by linear superposition. Hence, for the case where the potential V does not vary in time,
here is our solution of the time dependent equation.
We have our initial condition. We knew what this was. For some reason, we knew what this
was. We expanded it in the energy eigenstates that we could figure out. This is a complete
set.
Hence, expanding the wave function at time t equals zero in the energy eigenstates, we
have solved for the time evolution of the state just by adding up all of these terms.
There's no more work to do. If the potential is constant in time, it's really easy to solve
Schroedinger's time dependent equation if we know the solutions to the time independent
version.
Since a very large number of interesting problems do have potentials that are constant in time,
we therefore have a simple and very powerful way to look at the time evolution of quantum
mechanical systems.
We solve the eigenfunction problem with the time independent equation. Then we expand
our starting wave function in the complete set of these eigenfunctions. Then, we watch
the system evolve in time, just by adding up these solutions with their corresponding
time evolution complex exponentials.
Next we'll be looking at some examples and seeing how quantum mechanical systems evolve
in time. Remember that this entire time evolution can be viewed as a simple consequence of superposition
in quantum mechanics.