Now, there's an important contrast to the classical wave equation here. The classical
wave equation we're used to has a second spatial derivative on the left, but also a second,
in this case temporal, derivative on the right. And this wave is indeed a solution of the
classical wave equation we're used to-- this plane wave. But as I said, note that this
classical wave equation has a second time derivative, as opposed to the first time derivative
that we will find in our Schrodinger's time-dependent equation.
And also note that Schrodinger's use of a complex wave equation, so with an i over here--
there was no i anywhere in the classical equation-- means that generally, the wave psi that is
to be a solution of this equation has to be a complex entity. For example, for V equals
zero, although this wave is a solution-- e to the i kz minus Et over h, or that would
be minus omega t-- this wave, a sine wave, is not the solution.
For the classical wave equation, this could be a solution and this could be a solution.
But in this case of Schrodinger's equation, because he's made this introduction of complex
number-- and here the imaginary number i on the right-- in fact, although the complex
wave is a solution, a corresponding simple real wave is not a solution of the equation.
So far, it seems, we're just making our lives more complicated. We're not going to use the
most obvious wave equation for some reason that's not yet clear. We are also forcing
ourselves to use complex waves, which seems a significant price to pay. Now, in the classical
world, we often use complex waves as an algebraic convenience, but in the end, we get back to
the real world. We typically do this by adding a complex solution to its complex conjugate,
and the result of that gives us a real wave again.
In quantum mechanics, we do not do that. We live with the complex waves. Now, we might
think that those complex waves would cause us some difficulty in connecting back to reality,
because the real world is, well, real. In the end, we only use real numbers to describe
the real things in the real world. Remember two things, though, about the quantum mechanical
view. First, it's not clear that we expect the wave function itself to have any meaning.
We cannot apparently measure it. If you like, it's a some mathematical device that allows
us to calculate behavior. So the fact that it is complex maybe does not matter, in fact,
when we are thinking about how to connect to reality.
Second, when we want to extract measurable quantities from our calculations, we take
the modulus squared of the wave function, or more generally, the modulus squared of
the quantum mechanical amplitude. And that does get us back to the real world. We might
still ask, though, why are we paying this price of this new wave equation and its complex
wave? The answer to that lies in its ability to tell us what is going to happen next.
With the classical wave equation, if at some time we see a particular shape of wave-- for
example, a wave on a string, maybe like this pulse here-- and we've sort of taken a flash
picture, an instantaneous picture of this wave, so we see this shape here-- we don't
know if it's going to the right or if it's going to the left. In fact, it could even
be some combination of the two.
With the classical wave equation, knowledge of what the wave is now is not sufficient
on its own to tell us what will happen next. We would need additional information, such
as the time derivative of the wave. But with Schrodinger's time-dependent equation, just
from the form of the wave now, we do know what will happen next. That wave function
embodies everything we can know about the quantum mechanical state and consequently
what will happen to it in the future. That is why we are happy to pay the price of moving
away from the classical wave equation and from real waves to complex ones.
So let's look at how this works. So with Schrodinger's equation, if we have a known potential, V
of r and t, if we know the wave function psi of r and t 0-- that's the wave function at
every point in space at a specific time, t 0-- then we can evaluate this left-hand side
at that time for every position. And so we know the time derivative of the wave function
at every position.
So we could integrate this equation to deduce psi of r and t all future times. Explicitly
then, because we had known the wave at every position in space at a given time, we knew
the derivative at every position in space at a given time-- that's the time derivative--
so we can calculate the future wave. For example, at some time just a small instant of time,
delta t, later on, approximately speaking, the new wave would be the old wave plus the
derivative in time at each point in space times this little increment of time, delta
t.
In other words, from knowing the wave at this time, because we know the derivative, we can
multiply that derivative by a small amount of time to get the wave that small amount
of time later. So we can know the new wave function in space at the next instant in time,
and we can keep on doing this. We can do this again to find out the wave at the next instant
after that-- another delta t later, and so on. So we can predict all of the future evolution
of the wave function. Technically, all we're doing here is integrating this wave equation
in time.
We could view this ability to deduce the wave function at all future times as the reason
why Schrodinger's time-dependent equation has a first derivative in time, as opposed
to the second derivative in common classical wave equations. As we showed, knowing only
the second time derivative would not be sufficient to deduce the evolution in time.
Another key point about Schrodinger's time-independent equation is that any spatial function could
be a solution of the time-dependent Schrodinger equation at a given time, as long as it has
a finite and well-behaved second derivative. This is, again, very different from the time-independent
wave equation, which could only have very specific eigenfunctions as its solutions.
As we've seen, that spatial function-- the wave now, whatever it is-- sets a subsequent
time evolution of the wave function. At this point, we know in principle how to solve for
the time evolution of a wave function if we know what the wave function is everywhere
at one instant in time. There are many standard numerical techniques we could now use to perform
that integration in time of this equation, but we will not go into those here. Next,
though, we will see how to think about the solutions of the time-dependent equation and
how we can understand that time evolution quite simply.