The perturbation theory we've looked at so far has been for systems with fixed perturbations.
Nothing is changing in time. And we've been trying to evaluate new energy eigenstates
for our perturbed system. Now, that's a very useful theory, but it does not help us with
systems that are changing in time.
A very common example of a time varying perturbation might just be an oscillating perturbation,
such as an oscillating electric or electromagnetic field for example. This kind of situation
would occur every time we shine a light beam at a material for example, because we could
start out by thinking of a light beam as just an oscillating electromagnetic field.
Now, of course, if we have some time varying perturbation in a system, we can still solve
for what happens to the quantum mechanical state. We could just formally integrate the
Schroedinger equation. But it would be quite useful to have a theory that might give us
answers with less work than that. And we might reasonably expect that for relatively weak
perturbations to the systems, we might be able to construct just such as theory.
Time dependent perturbation theory is indeed just such an approach. Like time independent
perturbation theory, it's a theory of successive approximations, but now, for the time dependent
Schroedinger equation. This theory gives some quite simple results, and they're useful in
a broad range of quantum mechanical problems. One very good example is the absorption of
light by materials.
The light fields that we typically shine at materials are relatively weak compared to
the fields within the materials themselves. So this perturbation approach works well.
And the simplest, that is, the lowest order perturbation theory, is really good enough
for that. Incidentally, even for quite strong laser beams shining at materials or atoms,
perturbation theory still works well.
Higher order time dependent perturbation theory describes the resulting so-called non-linear
optical effects quite accurately. There, we get affects such as second harmonic generation
of light, which can turn an infrared invisible light beam into a visible one. And that's
an effect used quite often in those green laser pointers for example.
Now that we're used to perturbation theories in general, it's actually relatively easy
to introduce this time dependent version. Note that in time dependent perturbation theory,
we are trying to calculate something rather different from the results we were looking
for in time independent perturbation theory. There, we were trying to evaluate new versions
of energy eigenstate both in the energies of those perturbed states and in the wave
functions.
In time dependent perturbation theory, we're trying to predict how some system is going
to change in time. We'll therefore not have eigenstate or eigen energies in our results.
We will, however, still have wave functions, but now they will be changing in time. As
before, we'll examine wave functions by expanding on some basis set, with the most convenient
basis set typically being the energy eigen functions of the unperturbed problem. Those
will still form a very convenient starting point. And we already know that at some point
in time we can certainly expand any spatial function in them.
What we'll be calculating now is how the expansion coefficients of these basis functions change
in time. If we know that, then we can calculate the wave functions, or more generally, the
quantum mechanical state, at some later time. If we know the quantum mechanical state as
a function of time, then we can calculate any measurable quantity as a function of time.
Hence, our algebra will now be focused on those expansion coefficients and how we can
calculate them for small but time varying perturbations. So now let's set up the necessary
mathematics and the basic results.
For time dependent problems, we can consider some time dependent perturbation. So our perturbing
Hamiltonian here, that depends upon time. And we will consider that as an addition to
an unperturbed Hamiltonian, H naught here. And we'll presume that that Hamiltonian, the
unperturbed one, is not dependent on time.
The total Hamiltonian then is simply the sum of these two, the unperturbed one plus our
perturbing Hamiltonian that now, as we said, is explicitly going to depend upon time. To
deal with a situation like this, well, we just use the time dependent Schroedinger equation.
And this is written, of course, in the usual form here for the time dependent Schroedinger
equation. And the ket here, the psi, capital psi, is in general, time varying.
Now, we presumed that we could have solved the unperturbed problem where the Hamiltonian
was not depending upon time, and we could have deduced eigenstate functions and eigenvalues
of that particular time independent equation. So here is the classic time independent equation
here. And we can expand the solution, capital psi, of the time dependent Schroedinger equation
as some superposition of these unperturbed energy eigenstates here that are a complete
set for spatial functions. And rather deliberately here, we've put no time dependence inside
this particular ket, and we've taken out the usual e to the minus i Ent over h bar associated
with the eigenstate psi n. We put that explicitly here.
And then additionally, because this function is going to vary in time in some possibly
quite sophisticated way, we have also made our expansion coefficients dependent on time.
Now, there's nothing odd about this here. We're saying that simply at any point in time,
we could always expand the state capital psi in these eigenfunctions of the time independent
equation, because that could express any function in space at a given time. And if we make the
expansion coefficients depend upon time, then we can have a different solution at different
points in time. And as I said, we pulled this factor out here explicitly.
So as I said, we included this time dependent factor explicitly in the expansion, leaving
this time dependence here to deal only with additional changes that may be going on in
the system. So we've taken this one out, so we're not having to deal with it within the
an of t.
Now, we're going to substitute this expansion back into our time dependent Schroedinger
equation. And that will give us this larger expression here. And this a n dot here is
simply partial da and by dt. So we're using this notation with the dot, which is sometimes
easy to miss, but that's an notation we're going to use. So here's the an dot up here
that we got from this derivative.
Now, this H naught times psi n, of course, just gives us E n times psi n. And that means
you've got an En times psi n on both sides in this term. So we've got H naught psi n
that will turn into En psi n. We've got an En psi n on this side as well. And both of
them are multiplied by an, so we can cancel those out. So then we get a slightly simpler
equation which looks like this.
Now, we can pre multiply both sides of this equation that we've just derived by psi q,
some specific energy eigenfunction. And that on the left will pull out just the one term
from this summation here, the one corresponding to psi q.
On the right, it does something a bit more interesting. We have psi q acting on the left
here. Well, we're going to get a matrix element, psi q H p of t, times psi n. And that's exactly
what we have here. And now, we still have to perform this summation over n because in
general, these matrix elements are not going to be 0 for specific q and any n. They might
be 0, but they're not in general going to be 0.
Now, so far we've made no approximations at all. This statement here is merely a restatement
of Schroedinger's time dependent equation. It's still completely general. This equation
and this equation are actually quite equivalent provided we consider all possible qs here
and have a set of these equations.
Now we're going to consider our perturbation theories. So we're going to introduce our
expansion parameter, gamma, as before. We sometimes called it a housekeeping parameter.
And we're going to write our perturbation not as just Hp, but as before with the time
independent theory, we'll write it as gamma times Hp. And as before, if you like, we can
set the gamma equal to 1 at the end. In fact, it will usually cancel on both sides so the
issue doesn't arise.
We can now express our expansion coefficients as a power series. So as usual, we have these
superscripts 0, 1, 2, up here, corresponding to the gamma to the power of 0 term, the gamma
to the power of 1 term, and the gamma squared term, and so on. And we can substitute this
expansion into the expression that we've derived that we know is just equivalent to the time
dependent Schroedinger equation. Note that we have gamma Hp now instead of just H p.
Now, in this expression, we can equate powers of gamma on both sides. That's the way we're
going to construct our perturbation theory just as we did before. And we'll have these
expansions for aq and an. And the first thing we're going to do is equate terms in gamma
to the power of 0 on both sides of the equation.
Well, this is very easy. Because on the right hand side of the equation, for every term
there's at least a gamma to the power 1 in it. So on the right hand side, there are no
terms in gamma to the power 0. So the right hand side is 0.
On the left hand side, we have ih bar and we have this factor here, but those are just
numbers. In the power series expansion here for our time derivative, we are just going
to have, therefore, the gamma to the power of 0 term in here. We'll be looking at the
time dependence of the zero order coefficient on the left. But on the right we have 0, so
therefore, the time dependence of the 0 order coefficient is just, it doesn't depend on
time. The derivative is just equal to 0.
So the 0 order solution simply corresponds to the unperturbed solution. And this is the
same behavior that we had in the time independent perturbation theory before. And hence, there
is no change in the expansion coefficients in time to zero order. That's no surprise,
there's no perturbation involved with zero order.
So with this pair of theories here and our expression with the gamma inside, for the
first order term now, we're going to be equating terms in gamma to the power one 1 on the left
to terms in gamma to the power 1 on the right. Well, on the left, the term in gamma to the
power 1 is going to involve the first order coefficient here, an1. And on the right, since
we have our gamma built in to start with, then the only term that's going to survive
here in the expansion coefficients is going to be this zero order term.
So the first order term then becomes as follows. We take the ih bar and this complex exponential
over to the other side, so our first order term here is just associated with the first
order expansion coefficient connection. And on the right, we have the zero order expansion
coefficient, and everything else that we see here, and we've canceled the gammas.
Now, Incidentally, as we've done this, not only have we taken the ih bar over to the
other side, we've also taken this complex exponential factor over to the other side
and created a new notation for ourselves. And that notation is just to say this omega
qn is equal to Eq minus En over h bar. And here, we're trying to solve for what is the
expansion coefficient or the time dependence of the expansion coefficient to first order
for state q. So instead of these two terms here, first of all, we've taken this one over
to the right hand side and then we folded it into a new notation here, and omega qn
is just the difference in these energies divided by h bar.
Now, note in this expression that we now have, we already know that all of these are just
constants. They give us the starting state of the system at time t equals 0. We know
now that if we know the starting state and the perturbing potential, and we know the
unperturbed eigenvalues and eigenfunctions so these psi ns here and Ens that are folded
into this factor, we can integrate this equation in time. We're all set to do that now.
We would now be able to obtain the first order time dependent correction to the expansion
coefficients. And hence, actually, we would know to first order the change in the wave
function or the state of the system as a function of time.
So then, formally, after integrating this equation, we know the new approximate expansion
coefficients. They would be unperturbed ones plus these first order corrections we would
get from the integration in time of this equation. So we know the new wave function and then
calculate the behavior of the system for this new wave function.
We can if we want, at this point, proceed to a higher order in time dependent perturbation
theory. We could equate powers of progressively higher order to give us equations like this
where we'd be looking at the p plus 1 order on the left, and we would find that it depended
only on the p'th order corrections on the right. So we can see that this time dependent
perturbation theory is also a method of successive approximations just like the time independent
perturbation theory was. We can calculate each higher order correction from the preceding
connection.
Just as for the time independent perturbation theory, the time dependent theory is often
most useful for calculating some process to the lowest nonzero order. Fortunately, there's
a broad range of interesting and useful problems that can be addressed even by this first order
of time dependent perturbation theory. These include ordinary linear optical processes
and a wide range of interactions in materials such as the scattering of electrons or crystal
vibrations, so-called phonon scattering, and many other phenomena that influence the ordinary
movement of electrons and materials to carry electrical current for example.
Higher order time dependent perturbation theory is also quite useful, for example, in understanding
nonlinear optical processes as we mentioned earlier. So first order time dependent perturbation
theory gives the ordinary linear optical properties of materials. Higher order time dependent
perturbation theory is used to calculate these processes such as second harmonic generation,
two photon absorption, intensity dependent refractive index, and many other effects in
nonlinear optics, processes that are actually seen quite routinely with the high intensities
of modern lasers. Some are seen at quite low intensities in the long lengths of optical
fibers where from an engineering point of view, they represent significant constraints
on how well we can communicate over long distances.