We might think quantum mechanics is a strange subject, one that does not matter for our
daily lives. Maybe only a few people need concern themselves with it, such as some scientists
working on very basic topics like elementary particles. Or perhaps a few philosophers who
want to stare out of the window and wonder about free will.
Surely, we would not really see quantum mechanics around us in our everyday lives. And we might
not expect it to be useful for designing and making real things, things that can make real
money and change real lives. Of course, we would be wrong. Quantum mechanics is everywhere.
In this introduction, we're going to start by looking at some of the very early history
of quantum mechanics, and some of the ideas in quantum mechanics, and why we were driven
to that. Several of the people who started quantum mechanics really did not want things
to work out this way. But reality got in the way and forced them to have to rewrite science.
In this sense, quantum mechanics is astonishing. Nature really is not like the way we want
to think it is. As we go through this early development of quantum mechanics, we will
also begin to introduce some of the quantum mechanical ideas we will build on later. We're
going to look at how quantum mechanics impacts not only science, but also technology.
We now routinely use quantum mechanics in engineering. And without quantum mechanics,
we simply would not have much of the modern world as we know it. Finally, we're going
to briefly look at some of the truly bizarre aspects of quantum mechanics, ones that most
of us really do not start out by wanting to believe.
A key point here is that we're going to be honest, that there are some basic philosophical
problems in quantum mechanics, especially what is called the measurement problem. Now,
admitting this up front may make it easier for us to get on with learning the rest of
quantum mechanics. In the end, quantum mechanics is not actually that hard to use, though you
might not believe that at the moment, but it's true. And it works extremely well, even
if possibly no one actually quite understands it.