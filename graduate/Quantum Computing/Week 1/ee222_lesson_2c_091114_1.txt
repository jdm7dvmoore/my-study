We are used to oscillating systems in the classical world around us. All sorts of things
vibrate, such as musical instruments and loudspeakers. Many electronic circuits oscillate, such as
the devices that generate the microwaves for your microwave oven, or that create the radio
waves for radio transmissions, or for all those wireless remote controls you might have
around.
We're going to start here by looking at a very simple oscillator. Something that's called
the simple harmonic oscillator, which is the kind of oscillator we get if we have a mass
on a spring. Working through how this oscillator behaves also sets us up for thinking about
waves.
So after we've looked at this oscillator, we're going to go on to construct what we
call the classical wave equation, which is what explains, for example, waves on a string.
All of this discussion of waves is going to be very useful as we develop Schroedinger's
equation later on.
So let's look at a mass on a spring. A simple spring has a restoring force, F, acting on
this mass, capital M here. And the restoring force is proportional to the amount y by which
it is stretched. So we stretch this by some amount y in our y direction. And there's a
restoring force that is trying to lift the mass back up again.
And we typically refer to the amount of force we have here in terms of a spring constant
of some kind, constant K, which is just telling us how much force we get as we displace by
an amount y. Now, the minus sign here is very important. And it's because the force is a
restoring force.
If we extend by y in this direction, the force that's pointing in the opposite direction
is trending to reduce the amount of stretch that we put in, the amount we displace. It's
trying to take us back towards the equilibrium point we started at here. Taken together,
this gives a simple harmonic oscillator.
So let's look at the math of that. From Newton's second law, of course, force is equal to mass
times acceleration. Acceleration is the second derivative of position with respect to time.
So that's the force. And we know that our force, our restoring force from the spring,
is minus K times y, where K is our spring constant-- how difficult it is to stretch
it, basically.
So we can flip this around. And that gives us d2y by dt squared is equal to minus K over
M times y. And for the moment, we'll call that minus omega squared y. The reason for
that will become obvious very quickly. And we now have oscillatory solutions of this
equation. And if we look at those oscillatory solutions-- so that's kind of sine omega t,
or something like that-- they will have what we call an angular frequency omega, which
is equal to the square root of K over M.
So as I said, something like y is proportional to sine omega t is one of the possible solutions
of this. And we call this omega here the angular frequency. We could use the actual frequency
in Hertz, but then we'd have to put in a 2 pi f here. But we often prefer just to work
with what we call angular frequency. It just saves us some time in not writing down the
two pi's. So technically, the angular frequency is the number of radians per second, as opposed
to the number of cycles per second. But it just saves us a bit of pencil lead just to
use omega instead of f.
So let's watch this oscillate. It goes up and down in a nice sinusoidal motion here.
And we call this, as I said, a simple harmonic oscillator. So a physical system described
by an equation like this one, d2y by dt squared, is equal to minus some positive number here.
That's one of the aspects we get out of the omega squared. It's necessarily positive for
omega being real, minus some positive number times y. It's a simple harmonic oscillator.
And there are many, many examples of this. We see them all the time in the everyday world.
So a mass on a spring in many different forms is a simple harmonic oscillator. Electrical
resonant circuits often are simple harmonic oscillators. And there are many examples in
acoustics. And I'll show you one of those in a moment in what are called Helmholtz resonators.
And as I said, linear oscillators generally, these are all over the place. And so this
is a very useful kind of equation. It's very good that we understand this so simply.
I mentioned that one of the kinds of resonators that is a mass on a spring kind of a resonator,
a simple harmonic oscillator, is something called a Helmholtz resonator. This, believe
it or not, is a Helmholtz resonator. You blow across the top of it, it makes a noise. We
could call this the wine bottle resonator. But actually what it is is it's the air in
the neck of the bottle that's kind of like the mass.
And it bounces up and down on the spring, that is the air inside the bottle, kind of
like a balloon. And if I empty the water out of here, it will change the note of it because
it makes the balloon, or the spring down here, a little softer. So here's the note I have
at the moment. And if I take this little mug here and I pour the water out into it, because
the spring is now going to be somewhat softer, it's more easy to squeeze it, I should get
a different note.
So there we have it, a simple mass on a spring resonator. One point about this bottle is
that it will only produce one note. There's no way I can get another note out of it. And
that's one of the things that distinguishes it from another kind of resonator that we're
going to be looking at soon, which is a string, and waves on a string.