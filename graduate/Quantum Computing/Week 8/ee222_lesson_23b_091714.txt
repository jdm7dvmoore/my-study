Now we can calculate the various perturbation terms. We're going to start with our first
order equation here. And we're going to premultiply by psi m. Remember, we had decided that we
were going to look at perturbations to this specific state psi m.
So doing so, on the left-hand side here multiplying by psi m, on the left-hand side of this equation,
we can associate, if we like, with this parenthesis. Now, here we come across one of the very convenient
things above the Dirac notation. It's perfectly meaningful to consider H0 operating to the
left. Or if you like, this entire operator, H0 minus Em times the identity operator.
But this operating on the left just gives us Em times that wavefunction. So as a result
of working on this left-hand side here, we can rewrite that as follows And of course,
that's just zero, because we get Em minus Em in the middle. Now let's look at what happens
as a result of this premultiplication on the right-hand side of the equation.
Well, in that case, we've got psi m times E1 minus the perturbing Hamiltonian times
psi m. Well, psi m, E1, psi m is just E1 times psi m psi m-- which, of course, psi m psi
m gives us 1. So that's just E1.
And then we've got minus psi m Hp psi m. So that's this here. In other words, equating
this to 0 and moving things around, we have E1 is equal to psi m Hp psi m, a very simple
result. The first order of correction to the energy is just the matrix element of the perturbing
Hamiltonian for psi m psi m. And that's our formula for the first-order energy correction
E1 as a result of this perturbation Hp.
We can go on to calculate the first-order correction to the wavefunction, phi 1. What
we're going to do is expand that correction, this phi 1, in the eigenfunctions of the unperturbed
problem, psi n. So this is a whole set here.
Well, of course it's straightforward to write that down. We'll get some set of coefficients,
which we call an 1, multiplying each of the psi n. And we substitute this back into our
equation here. So we're going to substitute that expansion in here. And we're going to
premultiply by psi i.
On the left-hand side, that will give us psi i times H0 minus Em phi1. Well, H0 operating
on psi i is just Ei. And the psi i. So that becomes Ei minus Em in the middle here. We
can take that out as a number. And we're left with psi i phi1, with this number out in front.
But of course, this is just ai 1. So we're left with this result here. So that's what
happened multiplying with psi i on the left-hand side of this part of the equation. And now
we can deal with the right hand side.
So premultiplying the right-hand side by psi i gives us this here. And of course, we can
take the E1 out. That leaves us with E1 psi i psi m-- so that's this-- minus psi i Hp
psi m, this term here.
So with that expression we've just derived equating the two sides that we came up with
in our equation, we're now going to presume that the energy value Em, the unperturbed
energy value, is not degenerate. That is, there's only one eigenfunction for this eigenvalue.
With no degeneracy, we still need to distinguish two cases here. First of all, if i is not
equal to m from this equation that we have here, since we're not looking at degenerate
case, Ei is not equal to Em. And so we can rewrite this equation right away, dividing
on the right-hand side by Ei minus Em. So now we have an explicit formula for this expansion
coefficient.
But on the other hand, if i is equal to m, then going back up to our equation here, obviously,
with i equal to m, this is zero. And if we look at the right-hand side here, if i is
equal to m, then psi i psi m is just one. So we've got our first order energy correction
here. And we have this expression, i equal to m up here.
But that is just our expression for E1. So we've also got zero. So we've got zero equals
zero. Well, what's the point of that?
The point of that is that we have no constraints on what we choose for a m 1. Out algebra here
does not tell us what the a m 1 should be. Now, that might seem like bad news. But it
turns out that it's actually quite good news.
We are therefore free to choose this to be anything that is convenient. The choice that
makes our algebra simplest is simply to choose to set up our algebra taking this coefficient
to be zero. You might think we're doing something in the physics here. But we're really not.
We would just be adding in part of the original unperturbed wavefunction. That really has
no physical significance to it. We would end up having to re-scale all our algebras as
a result. So it's simpler just to choose this equal to 0.
We could construct theories with it not equal to 0, but there would be no benefit to it.
And there's no need for us to choose it to be anything other than 0. So that's the same
as saying that we choose to make the perturbation to the wavefunction orthogonal to the original
unperturbed wavefunction. That's what we're choosing.
There's really no physics to choosing to put a part in here that is not orthogonal to that.
We would construct a self-consistent theory, but there's no point in doing it. It would
give the same answers in the end, but there's no point in doing this.
Now, incidentally, if we look at the higher order equations, we will get exactly the same
phenomenon occurring. And we won't go through that algebra explicitly here, but the same
thing happens. So quite generally, what we're going to do in our perturbation theory is
to make the overall convenient choice that all of the perturbations to any order are
each orthogonal to the unperturbed wavefunction. We're always free to make that choice. And
it makes our algebra much more convenient.
So hence, with our expansion coefficients now worked out, we have this explicit formula
for any given expansion coefficient except for i equals m, and for the case of the expansion
coefficient of the original unperturbed wavefunction here we're choosing that to be 0.
So the first order correction to the wavefunction is given by this expression. Here are our
expansion coefficients multiplying these original basis functions here. The summation is over
all n's except n equal to m. We are explicitly excluding that. That's because of this statement
here.
And we have the first order correction already to the energy. And so these are the results
of first order perturbation theory. Therefore, to the extent that the change of the system
as a result of the perturbation is linear in the strength of the perturbation, we figured
out what both the changes in the wavefunction should be and what the changes in the energy
should be.
We can continue similarly to find the higher order terms. So we take our second order equation
and we premultiply on both sides by psi m. So on the left-hand side, we have psi m times
all of this. That's all of this here.
Of course, H0 operating on psi m is just Em psi m. So we've got this number in the middle
here, which of course is 0. So the left-hand side, as a result of this premultiplication
by psi m, just gives zero.
And then on the right-hand side, premultiplying by psi m, we have psi m times this here. This
is a number, of course. And psi m E 2 psi m. Well, this here, the E1 with these two
terms here, is E1 psi m phi 1. We have minus psi m Hp phi 1. That's this term.
And of course, psi m E 2 psi-m. Well psi-m psi-m is just 1. And E 2's just a number.
So this is E 2.
So E 2 is equal to psi m Hp phi 1. That's taking this term over to the other side. 0
here, so take that over to the other side. And we have this term also taken over to the
other side.
But we chose phi j to be orthogonal to psi m. We said that we would always find that
in our algebra if we followed it through, that we could make that choice. And so, therefore,
we're just left with these terms, that E 2 is equal to psi m Hp phi 1. So you see, incidentally,
that the second order energy correction depends upon the first order correction to the wavefunction.
If we use the result for our first order wavefunction correction, that we already figured out, which
was this expression here, we can put that back in here if we want and obtain a more
explicit expression for E 2. But of course, we can take this if we want inside the expression
here. Note that it's psi m Hp. This is just a number. So that's all really operating on
psi n.
So equivalently, therefore, we end up with the modulus squared of this matrix element
here. That is, we've got psi m, Hp, psi n times psi n Hp psi m. This is the complex
conjugate of psi m Hp psi n.
And so we have the modulus squared here. So we have an explicit expression for the second
order correction to the energy. Note, incidentally, that it's proportional to the square of the
magnitude of the perturbation, which makes some sense. For the second order wavefunction
correction, we expand phi 2, noting that phi 2 is chosen orthogonal to psi m.
So phi 2 is some coefficients, an 2 times psi n. And again, we are choosing n not equal
to m. We're not going through that algebra explicitly, but we would always find that
we could choose phi 2 to be orthogonal to the specific unperturbed function psi m.
Now we're going to premultiply our second order equation on both sides by psi i to obtain,
on the left, psi i H0 minus Em phi 2. Well, of course, H0 operating on psi i here just
gives us Ei. So again, this turns into Ei minus Em. And psi i phi 2 by definition is
ai 2.
Premultiplying on the right-hand side by psi i-- so we put psi i in front of each of these
two terms. Well, for the first one here, and the E1 part, we have psi i E1 phi 1. Well,
that is E1 times psi i phi 1. And that is just E1 ai 1, because psi i phi 1 is ai 1.
For this psi i Hp phi 1 with a minus sign in front of it, well, we can fold the psi
i Hp inside the expansion for phi 1. So that's what we've done here. And we have our minus
sign because we've had our minus sign.
For this term here, by definition, we're only working out coefficients where psi i is not
the same as psi m, where i is not the same as m. And that means that this term always
vanishes by choice, because we're never dealing with psi m psi m. It's back to this original
choice that phi 2 is going to be orthogonal to psi m.
And similarly, phi 1 is orthogonal to psi m. So we're never going to ask for the coefficient
where we chose to premultiply by psi m. So therefore, this term vanishes since that's
just a number. So we're left with just these two terms.
So finally then, we have from the left, this term here. From the right, all of these terms.
Note this summation, as before, excludes the term n equal to m because we chose this orthogonality.
Hence, for i not equal to m, we have this expression here. So i not equal to m. So that
this is not 0.
We take this over to the right-hand side. So this term here is from this one. And we've
flipped around the signs, incidentally. With Ei minus Em here, we've got Em minus Ei there.
So that's why that minus sign is gone. And it's why we have a minus sign in this term
here.
So we've got Em minus Ei here, where we had Ei minus Em up here. Note, incidentally, that
the second order wavefunction correction depends only on the first order energy and wavefunction.
So these an 1's and the ai 1 are coefficients that we get from the first order wavefunction
correction.
So let's summarize our first and second order perturbation results. For first order, we
have the energy correction is given by this expression and the wavefunction correction
is given by this one, where our expansion coefficients ai 1 are as follows, with the
exception that am 1 is chosen equal to 0.
And in second order, we have this simple expression for the second order correction, where we
have the first order wavefunction correction in here. And for the second order wavefunction
correction, we have it, of course, as an expansion on our basis set here, where these coefficients
are as follows, where again, we've chosen am 2 equal to zero. So these are the results
of first and second order perturbation theory. And these are the only formulae you need in
order to use it.