Now, we can do things with vectors mathematically. We can add them, for example. And adding vectors
graphically is quite straightforward. We just connect them head to tail in any order.
So for our vector G here, which is part of my final vector I'm going to end up with,
what I'm going to add is vector S together to get the vector G plus S. We see that we
just joined these two vectors head to tail here to get the final vector, G plus S.
And it does not matter what order we do this in. So we could start with the vector S and
then add the vector G, and we would still get the same vector, G plus S.
We can add vectors algebraically instead of graphically. And then the way to do it is
to add them component by component. So G plus S is this set of the three components of the
vector G, plus this set of the three components of the vector S. And then we add together
the components in each of the directions.
And we can see this graphically here. Here is our first component of the vector G along
the x direction. The direction of the unit vector I. Here is our component of the vector
S along the x direction. Note that in this particular case, it happens to be going in
the other direction. So the net vector, the resulting vector from adding these two components
along the x direction together, is this final vector here.
Then similarly, we can do the same thing in the y direction. So here's our component of
vector G in the y direction. Here's our component of vector S. We add the two of those together
to get the net vector in the direction G on the y-axis.
And then similarly, we are going to go along the z-axis now. And we have to remember we're
looking in a perspective diagram. So this is a z direction. This is a y direction. This
is an x direction.
So here's our component of vector G along the z direction. And then we add our component
of vector S along the z direction. And in this case, it happens to be going in the opposite
direction. So the net vector along the z direction is this short arrow here. And adding all those
together gives us our total vector, G plus S, that we have added component by component
to get our final result.
Now, having added vectors, we want to know if we can multiply vectors. And this turns
out to be somewhat more complicated. There are two kinds of multiplications, or products,
that we can do for geometrical vectors.
The first is called the dot product, a dot b. And that gives us a scalar result. It gives
us a simple number. The cross product, however, a cross b, gives us a vector. Now, note here,
in contrast to what we did with numbers, the dot here and the cross mean different kinds
of multiplication.
So let's look first at the vector dot product. A formula for the vector dot product is a
dot b is given by the modulus of a, which we'll see as the length, times the modulus
of b, which we'll see as a length of the b vector, times the cosine of the angle between
them. Or equivalently, a b cosine theta, where a and b are the lengths of the two vectors.
The modulus sign here means that we take the length of the vector a. We'll define that
as what we mean by this modulus notation, just as we used up here. And note, incidentally,
that a dot b is equal to b dot a. So this dot product is commutative. Also a dot a is
simply equal to a squared. And that means that the square root of a dot a is the length
of the vector a.
If we look at this product here, ab cos theta, we can see that we can regard it as the projection
of vector b onto direction a. So here's our line projecting down with a right angle in
the corner here, multiplied by the length of a, because b cos theta is the projection
of b onto a in terms of length.
Equivalently, we can regard it as the projection of vector a onto to the direction of vector
b multiplied by the length of b. We can see this formula can be looked at either way.
Note, incidentally, that for two vectors at right angles to one another, that is where
the angle here is pi by 2. Then cos of pi by 2 is equal to 0. And so the dot product
is equal to 0 as well.
So for all of these unit vectors, which are all orthogonal to one another, then all of
their dot products with one another are 0. So i dot j is equal to 0, because i and j
are orthogonal. And so on for all of the others of these dot products of vectors that are
orthogonal to one another, these unit vectors.
Also, since these are unit vectors, the dot product of a vector with itself is just 1.
Because all of these dot products are 0 for vectors with other ones of the unit vector
directions, we can form the dot product algebraically in the following way.
So we take a dot b. So it's this set of components here dot producted into this set of components.
But when we do that, all we're left with is ax times bx plus ay times by plus az times
bz. Because all of the cross terms, for example, axi dotted into byj, all go to 0 because i
dot j is equal to 0. So the dot product can be written in this form. It's a nice equivalent
algebraic form for the dot product.
The components of a vector, therefore, can be found by taking the dot product with the
unit vectors along the coordinate directions. So here we've got G dot producted with this
vector i here gives us the result Gx. The only term that survives out of this product
is the Gx itself.
Now, the other kind of vector product is called the vector cross product. And for two vectors,
which we'll write out here in terms of components. Two vectors with an angle theta between them.
The vector cross product, it looks superficially a little like the dot product. It's got the
magnitude of a and the magnitude of b. And instead of the cosine theta, it's got sine
theta.
But one very important difference is that this is a vector and there's a unit vector
n that multiplies this result. So we have ab sine theta, but it's a vector in the direction
n.
And you might ask, what's the direction of that vector? Well, the answer is, it's given
by what's called the right hand screw rule. The first and most important aspect of that
vector, though, is that it is orthogonal to both a and b. It's at right angles to both
a and b.
And if you look at this picture here, you will see that means it either has to point
into the picture or it has to point out of the picture. And which one of those it does
is determined by the right hand screw rule.
So here's a way of envisioning the right hand screw rule. We imagine we have our two vectors
here. And we're going to start out with a handle of a corkscrew lined up along the direction
of the vector a. Corkscrew is one of those objects that you can screw into the cork in
the top of a bottle, and when you've done that and you pull it, you can take the cork
out.
And then we imagine that we turn that handle so that it lines up now with the direction
b. The direction of the vector n is the direction that the corkscrew shaft moved in, the corkscrew
screw. So if, when we rotated from a to b, the corkscrew goes forwards into the diagram,
then the direction of the vector n is forwards into the diagram. And that's what would happen
with the right hand screw rule.
If, on the other hand, we were going in the other direction, if a was down here and b
was up there, then the result of rotating the corkscrew in this direction would actually
be to be taking it out of the cork. The direction n would then be pointing towards you, because
the corkscrew would be moving towards you.
So I have here a corkscrew. This is the usual implement you use to take the cork out of
a bottle. You screw it into the neck of the bottle, into the cork, and you pull to take
out the cork. Now, this has a right-handed thread, which means that as I rotate it clockwise
for me, it moves forward. If I rotate it anti-clockwise for me, it moves backward. This looks anti-clockwise
to you, and it looks as if it's moving backwards in towards you.
So this is the same no matter whether you're looking at it or I'm looking at it. It still
behaves as a right-hand screw. And the right hand screw rule for something like a vector
is that if I start out with this handle of the corkscrew oriented, say, along the vector
a, and I rotate that clockwise, for example, to get to some other vector, direction b,
the screw is moving in that direction. So the direction of the normal, the n vector,
is in this direction in the right hand screws rule, as I do a cross b.
If on the other hand, I was doing b cross a, I would start out in the b direction and
I would rotate it anti-clockwise to get back to the a direction. So that means, in that
case, the normal direction is pointing this way. The cross product would point in the
opposite direction if I do b cross a instead of a cross b.
Now, a very important part of the discussion of these vector cross products is that they
are not commutative. a cross b is equal to minus b cross a because of this screw rule
definition and the resulting direction of the vector n.
So if you have to turn clockwise to go from a to b, so the corkscrew goes in, then n points
inwards. And if we have to turn anti-clockwise to go from b to a, so the corkscrew goes out,
so n points outwards. And that is the sense in which a cross b is equal to minus b cross
a. It's because the vector direction n is pointing in the opposite direction in both
of those two cases.
We can write the vector cross product in an algebraic form. And I'll just state that for
you here. So in terms of the components of the vector, the resulting cross product here
is a vector. So we've got vector directions i, j, and k.
And note that the result for the length of the component pointing along the direction
i involves only the components in the other directions. So it involves the components
in the y and z directions to give us the length of the resulting cross product in the x direction.
And you see this behavior in each of the other components.
A shorthand way of writing this is to use the same notation as we use for what's called
the determinant in matrices. And at the moment, you could regard this statement here as being
equivalent to this one. If you know what matrix determinants look like, then you will understand
that these two are the same. But for the moment, we'll disregard this and this as being equivalent
notations. They are exactly equal.