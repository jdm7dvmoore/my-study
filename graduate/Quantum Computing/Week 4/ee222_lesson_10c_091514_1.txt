For people who have to work in detail with waves, such as people who work in optics,
the small dispersion, for example, that we have in glass, a very small change in refractive
index with frequency or wavelength, does give a significant effect in long fibers. And if
we go to interesting situations that physicists often look at, near atomic absorption lines,
for example, there's some very large changes in refractive index with frequency near there.
And they can give all sorts of interesting scientific effects, like very slow propagation
of light, for example.
And we often work technologically with wave guides in optical fibers or in other kinds
of integrated optical circuits. And in those different spatial shapes of waves, sometimes
called modes, do propagate at different velocities. That could be called a kind of dispersion
from geometry or structure. And any structure whose physical properties such as refractive
index are changing on a scale comparable with a wavelength, they're going to show very strong
dispersive affects as well. So in research, there's quite a lot of work in looking at
structures such as photonic crystals, where we pattern these structures on a sub-wavelength
scales, and we get very strong dispersive effects.
And also, there have been layered optical structures in existence for a long time in
special kinds of so-called dielectric stack structures that show quite strong and controllable
dispersion. But in general, just ordinary propagation of light that we see every day
is not really showing very strong group velocity effects. But we can see them if we look for
them.
Up till now, we've been looking at waves rather generally. And we've been noting that we can
certainly find this phenomenon of the group velocity being different from the phase velocity.
But in perhaps most situations with classical waves, this is typically a small effect or
one only important under relatively extreme conditions. But with quantum mechanical waves,
the situation is quite different.
In fact, for the quantum mechanical waves associated with something like an electron,
the phase velocity and the group velocity are almost never the same. So group velocity
is a very important concept for quantum mechanics. Without understanding it, we cannot even begin
to understand the movement of quantum mechanical particles like an electron.
Let's see why this is the case. For a free electron, the frequency omega is not proportional
to the wavevector magnitude k. For a free electron, that is one for which the potential
V is essentially constant, basically 0 everywhere, then we know what our Schroedinger equation
would look like. It becomes very simple. And we know the solutions to this equation.
They are basically e to the plus or minus ikz if we're looking at the spatial part.
The key point is that the energy here is proportional to k squared. And that means that if we look
at the frequency associated with that energy, which would be the energy divided by h bar,
we see that frequency is proportional to k squared and not proportional to k.
So we can calculate now the free electron group velocity from this expression. Remember,
the group velocity is d omega by dk. So we can differentiate this expression here, d
by dk of this.
And we could also write it out as 1 over h bar dE by dk. But this expression here differentiated
with respect to k, well, we're going to get 2k from the differentiation of the k squared.
So the net result of that differentiation is h bar k over m. Rather trivially, that's
the square root of h bar squared k squared over m squared.
And we can rearrange that a little bit. We can put a 2 in the top line and the bottom
line and separate out one of the m's. This h bar squared k squared over 2m, of course,
is just the energy, E.
So if we take this group velocity, which we've just calculated as the square root of 2E over
m, it does give us that the energy is 1/2m times the group velocity squared. And that
does correspond with our classical ideas of velocity and kinetic energy. This suggests
to us that it is meaningful to think of the electron as moving at the group velocity.
If we look at the phase velocity, however, the phase velocity is simply the ratio omega
over k. It does not give us that 1/2mv squared kind of relation. With energy equivalent to
h bar omega as before, and that's h bar squared k squared over 2m for our free electron, then
omega over k, which is our phase velocity, is har bar k over 2m.
And if we square that up, this all becomes 1 over 2m h bar squared k squared over 2m.
1 over 2m times E is what we get as the result. And that would give us the energy would be
twice the mass times the square of the phase velocity. It's not the 1/2mv squared if we
take v as being the phase velocity.
So the electron simply does not move at the phase velocity of the wave. It's not right.
The electron moves at the group velocity.