So now we understand that in situations where the potential in our problem is not itself
changing in time, we can simply calculate what is going to happen in time with our quantum
mechanical state. The point we are now at is that we have just proved that the following
process should work. We take the state we have now. We decompose it into a superposition
of the energy eigenstates. We let those eigenstates evolve in time with our complex exponential
factors, and we add up the result.
Now, we're going to look at that time evolution process with some important examples. We will
use two of the problems we've already solved-- the particle in a box and the harmonic oscillator.
We're going to start them off in specific superposition states, and we will see what
happens.