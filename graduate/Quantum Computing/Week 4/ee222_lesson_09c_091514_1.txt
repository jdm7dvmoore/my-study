The particle in a box has been a useful exercise here. And now we can expand these ideas to
the harmonic oscillator to see how it too can give us oscillation.
We're going to start off with a simple example. Just like the particle in a box example, we
could look at the superposition of the first two energy eigenstates. This is quite straightforward
to write down and quite simple to understand.
Quite generally if we're looking at superpositions, if we make a linear combination of two energy
eigenstates with energies E a and E b here, the resulting probability distribution is
going to oscillate at the angular frequency E a minus E b and then we take the modulus
here to get a positive frequency over h bar.
This is the frequency at which the oscillation will take place. So if we have a superposition
wave function of one particular function in space with its energy and some coefficient
c a here and another function in space with its energy and some coefficient c b here,
this is just a linear superposition of two states. Then the probability distribution
by the same algebra as we've just done will be this expression here, c a squared, times
this probability density c b squared times this probability density, plus this oscillating
term here that oscillates at the energy difference over h bar frequency.
Also we have angle in here, which is technically what we call the argument. The angle corresponding
to this particular complex number here. And that can vary in space in this particular
case. But the basic form is the same as we just saw. So with the harmonic oscillator,
we can work this out and as a reminder, here is what the harmonic oscillator wave functions
and energies look like for the first two states.
So the energy of the first state relative to the bottom of the parabolic potential was
h bar omega over 2. The energy of the second state was h bar omega over 2 times 3. In other
words, 1 and 1/2 h bar omega.
And this omega was the oscillation frequency, the angular oscillation frequency of the classical
oscillator with the same potential. So as we said here are the wave functions, one with
one bump in it and one with two bumps in it. So qualitatively, not so different from the
particle in a box problem. And again, in this case, we've been plotting them with these
orange dashed lines as the horizontal axes in each case.
So here's our n equals 0 in this case-- the lowest energy state-- with the wave function
now plotted with the horizontal axis here being the height equal to 0 axis. The bottom
of the potential well is where we've drawn our horizontal axis for the wave function
now.
And here's what the probability density looks like, the modulus squared of that function.
It's the same kind of shape, but it's just narrowed up a little bit. And again, if we
were to multiply this function by its time dependent factor and take the modulus squared
of it, the modulus squared, the probability density, is the same whether or not we multiply
by the time dependent factor. It makes no difference. The measurable quantity is the
same for this eigenstate, whether we multiply by the time dependent factor or not, just
as before.
Similarly, we can plot now the n equals 1 function, which is the second one in the harmonic
oscillator problem. And it goes down and up and back. And again, we've chosen this as
our horizontal axis now. And similarly, we can work out the probability density. Again,
very similar to the particle in the box problem qualitatively. It's all positive of course.
It's got two bumps in it. And again, if we multiply the spatial eigenfunction by the
time dependent factor so that it becomes a solution of the time dependent Schroedinger
equation, it makes no difference to the probability density.
An equal superposition of these two states is going to oscillate. And it's going to oscillate
at the angular frequency here. The difference between the lowest energy and the energy of
the next state, which here are called E 0 in the harmonic oscillator, n equals 0, and
E 1 for the next state up. And, as I said, we'll get an oscillation at this angular frequency.
And of course, here's the expression that we've just worked out and here's the oscillation
term down here. And here's what happens. So we're plotting here the sum of the two waves,
then modulus squared, which is what we have to do in quantum mechanics. And we're seeing
this oscillation here, that goes on.
So again, the particle is sort of oscillating between the left and the right and back again.
It's a slightly complicated looking little pattern in here. But we're certainly seeing
an oscillation between the right and the left and back again. And it's oscillating at this
frequency omega, which is, in this case, also the frequency of the classical harmonic oscillator
with the same potential. So our omega here, E 1 minus E 0 over h bar.
So now we have at last seen the harmonic oscillator oscillating. And at the frequency we were
expecting. Namely, the frequency of the classical harmonic oscillator with the same potential
energy function.
However, the behavior we are seeing here is still not very close to what we typically
see in a classical harmonic oscillator. In our next segment, we will see how to get that
behavior-- real harmonic oscillation from a quantum mechanical oscillator.